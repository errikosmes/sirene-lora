/*
 * main.h
 *
 *  Created on: 28 janv. 2021
 *      Author: quentin
 */

#ifndef APPLICATION_USER_MAIN_H_
#define APPLICATION_USER_MAIN_H_

#include "stm32l0xx_hal_tim.h"

void PWM_Set_Duty_Cycle(TIM_HandleTypeDef *htim, uint32_t duty_cycle);

#endif /* APPLICATION_USER_MAIN_H_ */
